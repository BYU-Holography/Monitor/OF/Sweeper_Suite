#include "outputApp.h"

//--------------------------------------------------------------
void outputApp::setup()
{
#ifdef TARGET_OPENGLES
  shader.load( "shadersES2/shader" ); // we're in Android / iOS ?
  ofLog() << "shadersES2 used";
#else
  if( ofIsGLProgrammableRenderer() )
  {
    shader.load( "shadersGL3/shader" ); // newer version of openGL
    ofLog() << "shadersGL3 (three) used"; // http://www.openframeworks.cc/documentation/utils/ofLog.html
  }
  else
  {
    shader.load( "shadersGL2/shader" ); // older version of openGL
    ofLog() << "shadersGL2 (two) used";
  }
#endif

  // give inital default values in case they're not changed and are needed again
  unfullscreened_window_state = { 100, 250, 400, 400 };

  // use a slight delay to wait to get the window state
  get_window_state_oneshot = 2;
}

//--------------------------------------------------------------
void outputApp::update()
{

}

//--------------------------------------------------------------
void outputApp::draw()
{
  // small test for reading data from the gui
  // ofSetColor( 255 * gui->gui0_RedAmp->getValue(), 255 * gui->gui0_GreenAmp->getValue(), 255 * gui->gui0_BlueAmp->getValue() );
  // ofRect( ofGetWidth() / 3, ofGetHeight() / 3, ofGetWidth() / 3, ofGetHeight() / 3 );

  ofSetColor( 255 ); // full opacity for the rectangle that is drawn with the shader
  shader.begin();

  shader.setUniform1i( "window_h", ofGetHeight() );

  shader.setUniform1i( "equalizer_enabled", gui->gui0_equalizerEnable->getChecked() ? 1 : 0 );
  shader.setUniform1i( "timeCoherent", gui->gui0_timeCoherent->getChecked() ? 1 : 0 );

  shader.setUniform1f( "vertGratingKR", ( gui->gui0_RedFreq->getValue() - gui->red_carrier_freq ) * K_SAMPLE_RATE );
  shader.setUniform1f( "vertGratingKG", ( gui->gui0_GreenFreq->getValue() - gui->green_carrier_freq ) * K_SAMPLE_RATE );
  shader.setUniform1f( "vertGratingKB", ( gui->gui0_BlueFreq->getValue() - gui->blue_carrier_freq ) * K_SAMPLE_RATE );

  shader.setUniform1f( "amplitudeR", gui->gui0_RedAmp->getValue() );
  shader.setUniform1f( "amplitudeG", gui->gui0_GreenAmp->getValue() );
  shader.setUniform1f( "amplitudeB", gui->gui0_BlueAmp->getValue() );

  shader.setUniform1f( "enableR", gui->gui0_RedEnable->getChecked() ? 1.0 : 0.0 );
  shader.setUniform1f( "enableG", gui->gui0_GreenEnable->getChecked() ? 1.0 : 0.0 );
  shader.setUniform1f( "enableB", gui->gui0_BlueEnable->getChecked() ? 1.0 : 0.0 );

  shader.setUniform1i( "outputMode", gui->output_mode );
  shader.setUniform1i( "outputChannel", gui->output_channel );
  shader.setUniform1i( "mixingMode", gui->shared_output_mixing_mode );

  ofRect( 0, 0, ofGetWidth(), ofGetHeight() ); // define the rectangle where the shader will draw
  shader.end();

  // use a slight delay to wait to get the window state
  if( get_window_state_oneshot > 0 )
  {
    get_window_state_oneshot--;

    if( get_window_state_oneshot == 1 && output_window->getWindowMode() == OF_WINDOW )
    {
      unfullscreened_window_state.x = output_window->getWindowPosition().x;
      unfullscreened_window_state.y = output_window->getWindowPosition().y;
      unfullscreened_window_state.w = output_window->getWindowSize().x;
      unfullscreened_window_state.h = output_window->getWindowSize().y;
    }
  }
}

//--------------------------------------------------------------
void outputApp::windowResized( int w, int h )
{
  printf( "output window %u now size [%ix%i] at pos <%i,%i>\n", output_window_index, w, h, ofGetWindowPositionX(), ofGetWindowPositionY() );
}

//--------------------------------------------------------------
void outputApp::keyPressed( int key )
{
  switch( key )
  {
  case 'f':
    // record the window state before going fullscreen
    // this is to have the correct window state to un-fullscreen to when loading states from a configuration file
    if( output_window->getWindowMode() == OF_WINDOW )
    {
      // the window is windowed, not fullscreened
      unfullscreened_window_state.x = output_window->getWindowPosition().x;
      unfullscreened_window_state.y = output_window->getWindowPosition().y;
      unfullscreened_window_state.w = output_window->getWindowSize().x;
      unfullscreened_window_state.h = output_window->getWindowSize().y;
    }

    ofToggleFullscreen();
    break;
  }
}

/*
//--------------------------------------------------------------
void outputApp::gotMessage( ofMessage msg )
{

}

//--------------------------------------------------------------
void outputApp::keyReleased(int key){

}

//--------------------------------------------------------------
void outputApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void outputApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void outputApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void outputApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void outputApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void outputApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void outputApp::dragEvent(ofDragInfo dragInfo){

}
*/

//--------------------------------------------------------------
void outputApp::exit()
{
  printf( "OutputApp %u: BYE.\n", output_window_index );

  // call GUI exit function to save state instead of immediately exiting out with std::exit( 0 );
  gui->exit();
}