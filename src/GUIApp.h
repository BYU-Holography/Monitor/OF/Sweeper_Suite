#pragma once

#include "ofMain.h"
#include "ofxDatGui.h"

#define _PI 3.141592653589793238462643383279

#define GPU_BW_MHZ 200.0
#define K_SAMPLE_RATE ( _PI / GPU_BW_MHZ )
// see "Constant Folding" compiler optimization

class AppGuiTheme : public ofxDatGuiTheme
{
public:
  AppGuiTheme()
  {
    font.size = 10;
    layout.width = 390;
    layout.labelWidth = 180;
    init();
  }
};

struct WindowState
{
  int x, y, w, h;
};

class outputApp; // forward declaration; see http://stackoverflow.com/questions/28697567/c-class-and-interlinked-objects-forming-a-cycle

class GUIApp : public ofBaseApp
{
public:

  std::vector<shared_ptr<outputApp>> *output_apps;
  shared_ptr<ofAppBaseWindow> gui_window;
  unsigned int output_window_count;

  ofxDatGui* gui0;

  ofxDatGuiToggle* gui0_equalizerEnable;
  ofxDatGuiToggle* gui0_timeCoherent;

  ofxDatGuiDropdown* gui0_SweepMode;
  int sweep_mode; // 0 - manual
                  // 1 - automatic
  ofxDatGuiDropdown* gui0_outputModeDD;
  int output_mode; // 0 - all three together
                   // 1 - all three separate
  ofxDatGuiDropdown* gui0_outputChannelDD;
  int output_channel; // 0 - all three on R
                      // 1 - all three on G
                      // 2 - all three on B
  ofxDatGuiDropdown* gui0_mixingModeDD;
  int shared_output_mixing_mode; // 0 - average with constant divisor
                                 // 1 - average with changing divisor based upon sum of amplitudes

  ofxDatGuiButton* gui0_StopSweep;
  ofxDatGuiSlider* gui0_SweepRate;
  double sweep_rate;
  ofxDatGuiSlider* gui0_SweepPercentage;
  double sweep_percentage;

  ofxDatGuiToggle* gui0_RedEnable;
  ofxDatGuiTextInput* gui0_RedCarrierFrequency;
  double red_carrier_freq;
  ofxDatGuiSlider* gui0_RedMinFreq;
  double red_min_freq;
  ofxDatGuiSlider* gui0_RedFreq;
  ofxDatGuiSlider* gui0_RedMaxFreq;
  double red_max_freq;
  ofxDatGuiSlider* gui0_RedAmp;

  ofxDatGuiToggle* gui0_GreenEnable;
  ofxDatGuiTextInput* gui0_GreenCarrierFrequency;
  double green_carrier_freq;
  ofxDatGuiSlider* gui0_GreenMinFreq;
  double green_min_freq;
  ofxDatGuiSlider* gui0_GreenFreq;
  ofxDatGuiSlider* gui0_GreenMaxFreq;
  double green_max_freq;
  ofxDatGuiSlider* gui0_GreenAmp;

  ofxDatGuiToggle* gui0_BlueEnable;
  ofxDatGuiTextInput* gui0_BlueCarrierFrequency;
  double blue_carrier_freq;
  ofxDatGuiSlider* gui0_BlueMinFreq;
  double blue_min_freq;
  ofxDatGuiSlider* gui0_BlueFreq;
  ofxDatGuiSlider* gui0_BlueMaxFreq;
  double blue_max_freq;
  ofxDatGuiSlider* gui0_BlueAmp;

  ofxDatGuiSlider* gui0_lastSlider;

  void carrier_freq_change_helper( ofxDatGuiSlider* s, double carrier_freq );
  void onSliderEvent_helper( ofxDatGuiSlider* slider );

  void onButtonEvent( ofxDatGuiButtonEvent e );
  void onToggleEvent( ofxDatGuiToggleEvent e );
  void onTextInputEvent( ofxDatGuiTextInputEvent e );
  void onSliderEvent( ofxDatGuiSliderEvent e );
  void onDropdownEvent( ofxDatGuiDropdownEvent e );

  void MinMaxSetSweepPercentage( double new_percentage );

  ofTrueTypeFont font;

  void setup();
  void update();
  void draw();

  bool shift_modifier;
  bool alt_modifier;
  bool ctrl_modifier;
  bool left_mouse_button;
  bool is_adjusting_min_max_freq;

  void keyPressed( int key );
  void keyReleased( int key );
  //void mouseMoved( int x, int y );
  //void mouseDragged( int x, int y, int button );
  void mousePressed( int x, int y, int button );
  void mouseReleased( int x, int y, int button );
  //void mouseEntered( int x, int y );
  //void mouseExited( int x, int y );
  void windowResized( int w, int h );
  //void dragEvent( ofDragInfo dragInfo );
  //void gotMessage( ofMessage msg );
  void exit();

  // save / load state; related to startup / exit
  void SaveState( string file_path );
  bool LoadState( string file_path );
  int load_autosave_oneshot;
};
