#pragma once

#include "ofMain.h"
#include "GUIApp.h"

class outputApp : public ofBaseApp
{

public:

  unsigned int output_window_index;

  shared_ptr<GUIApp> gui;
  shared_ptr<ofAppBaseWindow> output_window;

  // save window state before it's fullscreened
  WindowState unfullscreened_window_state;
  int get_window_state_oneshot;  // use a slight delay to wait to get the window state

  ofShader shader;

  void setup();
  void update();
  void draw();

  void keyPressed( int key );
  void windowResized( int w, int h );
  //void keyReleased( int key );
  //void mouseMoved( int x, int y );
  //void mouseDragged( int x, int y, int button );
  //void mousePressed( int x, int y, int button );
  //void mouseReleased( int x, int y, int button );
  //void mouseEntered( int x, int y );
  //void mouseExited( int x, int y );
  //void dragEvent( ofDragInfo dragInfo );
  //void gotMessage( ofMessage msg );
  void exit();
};
