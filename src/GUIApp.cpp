#include "GUIApp.h"
#include "outputApp.h"

//--------------------------------------------------------------
void GUIApp::setup()
{
  ofxDatGui::setAssetPath( "/" );
  // change ofxDatGui asset path from 
  //   "../../../../../addons/ofxDatGui/" ("of_v0.9.3_vs_release/addons/ofxDatGui/")
  // to
  //   "Sweeper_Suite/bin/data/"
	gui0 = new ofxDatGui( 0, 0 );
	gui0->setTheme( new AppGuiTheme() );

  gui0_equalizerEnable = gui0->addToggle("Enable Equalizer", false);
  gui0_timeCoherent = gui0->addToggle( "Time-Coherent", false );

	sweep_mode = 1; // 0 - manual
                  // 1 - automatic
	vector<string> sweep_options = { "Sweep: Manual","Sweep: Auto" };
	gui0_SweepMode = gui0->addDropdown( "Sweep Mode", sweep_options );
	gui0_SweepMode->select( sweep_mode );

  gui0_StopSweep = gui0->addButton( "[ Stop Sweep ]" );

	sweep_rate = 0.25;
	gui0_SweepRate = gui0->addSlider( "Sweep Rate", -2.0, 2.0 );
	gui0_SweepRate->setValue( sweep_rate );
  gui0_SweepRate->setVisible( true );

	sweep_percentage = 0.5;
	gui0_SweepPercentage = gui0->addSlider( "Sweep Alpha", 0.0, 1.0 );
	gui0_SweepPercentage->setValue( sweep_percentage );
  gui0_SweepPercentage->setVisible( true );


	vector<string> mode_options = { "Output Mode: Shared", "Output Mode: Separate" };
	gui0_outputModeDD = gui0->addDropdown( "Output Mode", mode_options );
	output_mode = 1; // 0 - all three together
                   // 1 - all three separate
	gui0_outputModeDD->select( output_mode );


	vector<string> channel_options = { "Output Channel: Red", "Output Channel: Green", "Output Channel: Blue" };
	gui0_outputChannelDD = gui0->addDropdown( "Output Channel", channel_options );
	output_channel = 0; // 0 - all three on R
                      // 1 - all three on G
                      // 2 - all three on B
	gui0_outputChannelDD->select( output_channel );
  gui0_outputChannelDD->setVisible( output_mode == 0 );

	vector<string> mixing_options = { "Averaging Divisor: Constant", "Averaging Divisor: Summation" };
	gui0_mixingModeDD = gui0->addDropdown( "Shared Mixing Mode", mixing_options );
	shared_output_mixing_mode = 0; // 0 - average with constant divisor
                                 // 1 - average with changing divisor based upon sum of amplitudes
  gui0_mixingModeDD->select( shared_output_mixing_mode );
  gui0_mixingModeDD->setVisible( output_mode == 0 );


  char temp_c_str[16];

	gui0_RedEnable                    = gui0->addToggle( "Red Enable", true );
  red_carrier_freq = 350.0;
  sprintf( temp_c_str, "%.2f", red_carrier_freq );
  gui0_RedCarrierFrequency = gui0->addTextInput( "Red Carrier Freq", temp_c_str );
  gui0_RedCarrierFrequency->setInputType( ofxDatGuiInputType::NUMERIC );
	gui0_RedMinFreq                   = gui0->addSlider( "Red Min Freq",   red_carrier_freq, red_carrier_freq + GPU_BW_MHZ, red_carrier_freq );
	gui0_RedFreq                      = gui0->addSlider( "Red Freq",       red_carrier_freq, red_carrier_freq + GPU_BW_MHZ, red_carrier_freq + GPU_BW_MHZ * 0.5 );
	gui0_RedMaxFreq                   = gui0->addSlider( "Red Max Freq",   red_carrier_freq, red_carrier_freq + GPU_BW_MHZ, red_carrier_freq + GPU_BW_MHZ );
	gui0_RedAmp                       = gui0->addSlider( "Red Amp", 0.0, 1.0, 1.0 );

	gui0_GreenEnable = gui0->addToggle( "Green Enable", false );
  green_carrier_freq = 0.0;
  sprintf( temp_c_str, "%.2f", green_carrier_freq );
  gui0_GreenCarrierFrequency = gui0->addTextInput( "Green Carrier Freq", temp_c_str );
  gui0_GreenCarrierFrequency->setInputType( ofxDatGuiInputType::NUMERIC );
	gui0_GreenMinFreq                 = gui0->addSlider( "Green Min Freq", green_carrier_freq, green_carrier_freq + GPU_BW_MHZ, 0.0 );
	gui0_GreenFreq                    = gui0->addSlider( "Green Freq",     green_carrier_freq, green_carrier_freq + GPU_BW_MHZ, 0.0 );
	gui0_GreenMaxFreq                 = gui0->addSlider( "Green Max Freq", green_carrier_freq, green_carrier_freq + GPU_BW_MHZ, 0.0 );
	gui0_GreenAmp                     = gui0->addSlider( "Green Amp", 0.0, 1.0, 1.0 );

	gui0_BlueEnable  = gui0->addToggle( "Blue Enable", true );
  blue_carrier_freq = 0.0;
  sprintf( temp_c_str, "%.2f", blue_carrier_freq );
  gui0_BlueCarrierFrequency = gui0->addTextInput( "Blue Carrier Freq", temp_c_str );
  gui0_BlueCarrierFrequency->setInputType( ofxDatGuiInputType::NUMERIC );
	gui0_BlueMinFreq                  = gui0->addSlider( "Blue Min Freq",  blue_carrier_freq, blue_carrier_freq + GPU_BW_MHZ, 1.0 );
	gui0_BlueFreq                     = gui0->addSlider( "Blue Freq",      blue_carrier_freq, blue_carrier_freq + GPU_BW_MHZ, 1.0 );
	gui0_BlueMaxFreq                  = gui0->addSlider( "Blue Max Freq",  blue_carrier_freq, blue_carrier_freq + GPU_BW_MHZ, 1.0 );
	gui0_BlueAmp                      = gui0->addSlider( "Blue Amp", 0.0, 1.0, 1.0 );



  gui0_SweepRate->setPrecision( 2, false );
  gui0_SweepPercentage->setPrecision( 2, false );
  gui0_RedMinFreq->setPrecision( 2, false );
  gui0_RedFreq->setPrecision( 2, false );
  gui0_RedMaxFreq->setPrecision( 2, false );
  gui0_RedAmp->setPrecision( 2, false );
  gui0_GreenMinFreq->setPrecision( 2, false );
  gui0_GreenFreq->setPrecision( 2, false );
  gui0_GreenMaxFreq->setPrecision( 2, false );
  gui0_GreenAmp->setPrecision( 2, false );
  gui0_BlueMinFreq->setPrecision( 2, false );
  gui0_BlueFreq->setPrecision( 2, false );
  gui0_BlueMaxFreq->setPrecision( 2, false );
  gui0_BlueAmp->setPrecision( 2, false );



	gui0_RedMinFreq->setVisible( true );
	gui0_RedMaxFreq->setVisible( true );
	gui0_GreenMinFreq->setVisible( true );
	gui0_GreenMaxFreq->setVisible( true );
	gui0_BlueMinFreq->setVisible( true );
	gui0_BlueMaxFreq->setVisible( true );


  gui0_lastSlider = gui0_SweepPercentage;


  gui0->onButtonEvent( this, &GUIApp::onButtonEvent );
	gui0->onToggleEvent( this, &GUIApp::onToggleEvent );
  gui0->onTextInputEvent( this, &GUIApp::onTextInputEvent );
	gui0->onSliderEvent( this, &GUIApp::onSliderEvent );
	gui0->onDropdownEvent( this, &GUIApp::onDropdownEvent );



	alt_modifier = false;
	shift_modifier = false;
	ctrl_modifier = false;

  is_adjusting_min_max_freq = false;


  // load fonts

  //ofTrueTypeFont::setGlobalDpi( 72 ); // this screws up the font size

  font.load("FAFERS_Technical_Font.ttf", 14, false);
  if( !font.isLoaded() )
    font.load( "ofxbraitsch/fonts/Verdana.ttf", 12, false );
  if (!font.isLoaded())
    font.load("arial.ttf", 14, false);
  font.setLineHeight(font.getSize() * 1.125);
  font.setLetterSpacing(1.037);


  // load state
  // see void GUIApp::exit() description
  //LoadState( "autosave.json" ); // this doesn't work in setup(); use a trigger in update()
  load_autosave_oneshot = 2;
}

//--------------------------------------------------------------
void GUIApp::onButtonEvent( ofxDatGuiButtonEvent e )
{
  printf( "\"%s\" pressed\n", e.target->getLabel().c_str() );

  if( e.target == gui0_StopSweep )
  {
    sweep_rate = 0.0;
    gui0_SweepRate->setValue( sweep_rate );
  }
}

//--------------------------------------------------------------
void GUIApp::carrier_freq_change_helper( ofxDatGuiSlider* s, double carrier_freq )
{
  double x = s->getValue();

  if( carrier_freq < s->getMin() )
    s->setMin( carrier_freq );

  if( s->getMax() < carrier_freq )
    s->setMax( carrier_freq + GPU_BW_MHZ );

  s->setMin( carrier_freq );
  s->setMax( carrier_freq + GPU_BW_MHZ );

  if( x < carrier_freq )
    s->setValue( s->getMin() );

  if( carrier_freq + GPU_BW_MHZ < x )
    s->setValue( s->getMax() );
}

//--------------------------------------------------------------
void GUIApp::onTextInputEvent( ofxDatGuiTextInputEvent e )
{
  printf( "\"%s\": %s\n", e.target->getLabel().c_str(), e.text.c_str() );

  if( e.target == gui0_RedCarrierFrequency )
  {
    double x = atof( e.text.c_str() );

    if( x < 0.0 )
    {
      x = 0.0;

      char temp_c_str[32];

      sprintf( temp_c_str, "%.1f", x );
      e.target->setText( temp_c_str );
    }

    red_carrier_freq = x;

    carrier_freq_change_helper( gui0_RedMinFreq, red_carrier_freq );
    carrier_freq_change_helper( gui0_RedMaxFreq, red_carrier_freq );
    carrier_freq_change_helper( gui0_RedFreq, red_carrier_freq );
  }

  if( e.target == gui0_GreenCarrierFrequency )
  {
    double x = atof( e.text.c_str() );

    if( x < 0.0 )
    {
      x = 0.0;

      char temp_c_str[32];

      sprintf( temp_c_str, "%.1f", x );
      e.target->setText( temp_c_str );
    }

    green_carrier_freq = x;

    carrier_freq_change_helper( gui0_GreenMinFreq, green_carrier_freq );
    carrier_freq_change_helper( gui0_GreenMaxFreq, green_carrier_freq );
    carrier_freq_change_helper( gui0_GreenFreq, green_carrier_freq );
  }

  if( e.target == gui0_BlueCarrierFrequency )
  {
    double x = atof( e.text.c_str() );

    if( x < 0.0 )
    {
      x = 0.0;

      char temp_c_str[32];

      sprintf( temp_c_str, "%.1f", x );
      e.target->setText( temp_c_str );
    }

    blue_carrier_freq = x;

    carrier_freq_change_helper( gui0_BlueMinFreq, blue_carrier_freq );
    carrier_freq_change_helper( gui0_BlueMaxFreq, blue_carrier_freq );
    carrier_freq_change_helper( gui0_BlueFreq, blue_carrier_freq );
  }
}

//--------------------------------------------------------------
void GUIApp::onToggleEvent( ofxDatGuiToggleEvent e )
{
	printf( "\"%s\": %s\n", e.target->getLabel().c_str(), e.target->getChecked() ? "TRUE" : "FALSE" );
}

void GUIApp::MinMaxSetSweepPercentage( double new_percentage )
{
  is_adjusting_min_max_freq = true;

  sweep_percentage = new_percentage;

  if( sweep_percentage > 1.0 )
    sweep_percentage = 1.0;

  if( sweep_percentage < 0.0 )
    sweep_percentage = 0.0;

  gui0_SweepPercentage->setValue( sweep_percentage );
}

//--------------------------------------------------------------
void GUIApp::onSliderEvent_helper( ofxDatGuiSlider* slider )
{
  // I would use a switch-case statement, but the case #'s have to be known before compiling
  if( slider == gui0_SweepRate )
  {
    sweep_rate = gui0_SweepRate->getValue();
  }
  else if( slider == gui0_SweepPercentage )
  {
    sweep_percentage = gui0_SweepPercentage->getValue();
  }
  else if( slider == gui0_RedMinFreq )
    MinMaxSetSweepPercentage( 0.0 ); // set the sweep alpha to 0% while adjusting minimum frequencies
  else if( slider == gui0_RedMaxFreq )
    MinMaxSetSweepPercentage( 1.0 ); // set the sweep alpha to 100% while adjusting maximum frequencies
  else if( slider == gui0_GreenMinFreq )
    MinMaxSetSweepPercentage( 0.0 );
  else if( slider == gui0_GreenMaxFreq )
    MinMaxSetSweepPercentage( 1.0 );
  else if( slider == gui0_BlueMinFreq )
    MinMaxSetSweepPercentage( 0.0 );
  else if( slider == gui0_BlueMaxFreq )
    MinMaxSetSweepPercentage( 1.0 );
}

void GUIApp::onSliderEvent( ofxDatGuiSliderEvent e )
{
  // printf( "\"%s\": %f", e.target->getLabel().c_str(), e.value );

  gui0_lastSlider = e.target;
  
  onSliderEvent_helper( e.target );

	// printf( " gui_widget_focused_index = %u\n", gui_widget_focused_index );
}




void GUIApp::onDropdownEvent( ofxDatGuiDropdownEvent e )
{
	string newDDLabel = e.target->getLabel();

	//printf( "%s\n", newDDLabel.c_str() );

	if( e.target == gui0_SweepMode )
	{
		sweep_mode = e.child;

		printf( "Sweep Mode : " );

		switch( sweep_mode )
		{
		case 0:
			printf( "Manual\n" );

      gui0_StopSweep->setVisible( false );
			gui0_SweepRate->setVisible( false );
			gui0_SweepPercentage->setVisible( false );

			gui0_RedMinFreq->setVisible( false );
			gui0_RedMaxFreq->setVisible( false );
			gui0_GreenMinFreq->setVisible( false );
			gui0_GreenMaxFreq->setVisible( false );
			gui0_BlueMinFreq->setVisible( false );
			gui0_BlueMaxFreq->setVisible( false );

			break;
		case 1:
			printf( "Auto\n" );

      gui0_StopSweep->setVisible( true );
			gui0_SweepRate->setVisible( true );
			gui0_SweepPercentage->setVisible( true );

			gui0_RedMinFreq->setVisible( true );
			gui0_RedMaxFreq->setVisible( true );
			gui0_GreenMinFreq->setVisible( true );
			gui0_GreenMaxFreq->setVisible( true );
			gui0_BlueMinFreq->setVisible( true );
			gui0_BlueMaxFreq->setVisible( true );

			break;
		}
	}
	else if( e.target == gui0_outputModeDD )
	{
		output_mode = e.child;

		printf( "Mode: %i\n", output_mode );

		if( output_mode == 1 )
		{
		  // all three separate
			gui0_outputChannelDD->setVisible( false );
			gui0_mixingModeDD->setVisible( false );
		}
		else
		{
		  // all three combined
			gui0_outputChannelDD->setVisible( true );
			gui0_mixingModeDD->setVisible( true );
		}
	}
	else if( e.target == gui0_outputChannelDD )
	{
		output_channel = e.child;

		printf( "Channel: %i\n", output_channel );
	}
	else if( e.target == gui0_mixingModeDD )
	{
		shared_output_mixing_mode = e.child;

		printf( "Shared Mixing Mode: %i\n", shared_output_mixing_mode );
	}

  // resize window if GUI options changed (make life simple by always resizing it, even if it's the same size)
  gui_window->setWindowShape( gui0->getWidth(), gui0->getHeight() + font.getLineHeight() );
}


//--------------------------------------------------------------
void GUIApp::update()
{

	if( sweep_mode == 1 )
	{
		// auto-sweep mode

    // if we're not adjusting the Min / Max frequency sliders...
    if( sweep_rate != 0.0 && !is_adjusting_min_max_freq )
    {
      sweep_percentage += sweep_rate / ofGetFrameRate(); // normalize sweep rates regardless of framerate

      if( sweep_percentage > 1.0 )
        sweep_percentage = 0.0;

      if( sweep_percentage < 0.0 )
        sweep_percentage = 1.0;

      gui0_SweepPercentage->setValue( sweep_percentage );
    }
    
    red_min_freq   = gui0_RedMinFreq->getValue();
    red_max_freq   = gui0_RedMaxFreq->getValue();
    green_min_freq = gui0_GreenMinFreq->getValue();
    green_max_freq = gui0_GreenMaxFreq->getValue();
    blue_min_freq  = gui0_BlueMinFreq->getValue();
    blue_max_freq  = gui0_BlueMaxFreq->getValue();

    gui0_RedFreq->setValue( sweep_percentage * ( red_max_freq - red_min_freq ) + red_min_freq );
    gui0_GreenFreq->setValue( sweep_percentage * ( green_max_freq - green_min_freq ) + green_min_freq );
    gui0_BlueFreq->setValue( sweep_percentage * ( blue_max_freq - blue_min_freq ) + blue_min_freq );
	}
}

//--------------------------------------------------------------
void GUIApp::draw()
{
  // gui draws itself

  // draw GUI FPS

  float font_line_height = font.getLineHeight();

  ofSetColor(64, 64, 64);
  ofRect(ofGetWidth() - 9 * font_line_height, ofGetHeight() - 1 * font_line_height, 9 * font_line_height, 1 * font_line_height);

  char temp_c_array[32];
  sprintf(temp_c_array, "FPS: %.1f", ofGetFrameRate() );
  ofSetColor(255, 255, 255);
  font.drawString(temp_c_array, ofGetWidth() - 9 * font_line_height, ofGetHeight() - 0 * font_line_height);

  // load state after window has finished initializing
  if( load_autosave_oneshot > 0 )
  {
    load_autosave_oneshot--;

    if( load_autosave_oneshot == 1)
      LoadState( "autosave.json" );
  }
}

//--------------------------------------------------------------
void GUIApp::keyPressed( int key )
{
	switch( key )
	{
	case OF_KEY_ALT: // key modifiers: https://github.com/openframeworks/openFrameworks/issues/2315
		alt_modifier = true;
		// printf( "alt pressed\n" );
		break;
	case OF_KEY_SHIFT:
		shift_modifier = true;
		// printf( "shift pressed\n" );
		break;
	case OF_KEY_CONTROL:
		ctrl_modifier = true;
		// printf( "ctrl pressed\n" );
		break;

	case OF_KEY_LEFT:
	case OF_KEY_RIGHT:
		double value = gui0_lastSlider->getValue();
    double max = gui0_lastSlider->getMax();
    double min = gui0_lastSlider->getMin();

		double delta = 0.01*( max - min );

		if( ctrl_modifier )// each ALT, SHIFT, CTRL modifier decreases the rate by 1/10
			delta *= 0.1;    // the more modifiers, the smaller the adjustmet
		if( shift_modifier )
			delta *= 0.1;
		if( alt_modifier )
		  delta *= 0.1;

		switch( key )
		{
		case OF_KEY_LEFT:
			value -= delta;
			break;
		case OF_KEY_RIGHT:
			value += delta;
			break;
		}

		if( value > max )
			value = max;
		if( value < min )
			value = min;

    gui0_lastSlider->setValue( value );

    onSliderEvent_helper( gui0_lastSlider );

		break;
	}
}

//--------------------------------------------------------------
void GUIApp::keyReleased( int key )
{
	switch( key )
	{
	case OF_KEY_ALT:
		alt_modifier = false;
		// printf( "alt released\n" );
		break;

	case OF_KEY_SHIFT:
		shift_modifier = false;
		// printf( "shift released\n" );
		break;

	case OF_KEY_CONTROL:
		ctrl_modifier = false;
		// printf( "ctrl released\n" );
		break;

  case OF_KEY_LEFT:
  case OF_KEY_RIGHT:

    if( gui0_lastSlider == gui0_RedMinFreq || gui0_lastSlider == gui0_RedMaxFreq ||
        gui0_lastSlider == gui0_GreenMinFreq || gui0_lastSlider == gui0_GreenMaxFreq ||
        gui0_lastSlider == gui0_BlueMinFreq || gui0_lastSlider == gui0_BlueMaxFreq )
      is_adjusting_min_max_freq = false;

    break;
	}
}

//--------------------------------------------------------------
void GUIApp::windowResized( int w, int h )
{

}

/*
//--------------------------------------------------------------
void GUIApp::mouseMoved( int x, int y )
{

}

//--------------------------------------------------------------
void GUIApp::mouseDragged( int x, int y, int button )
{

}
*/
//--------------------------------------------------------------
void GUIApp::mousePressed( int x, int y, int button )
{
  //if( button == OF_MOUSE_BUTTON_LEFT )
  //  left_mouse_button = true;
}

//--------------------------------------------------------------
void GUIApp::mouseReleased( int x, int y, int button )
{
  if( button == OF_MOUSE_BUTTON_LEFT )
    is_adjusting_min_max_freq = false;
}
/*
//--------------------------------------------------------------
void GUIApp::mouseEntered( int x, int y )
{

}

//--------------------------------------------------------------
void GUIApp::mouseExited( int x, int y )
{

}

//--------------------------------------------------------------
void GUIApp::gotMessage( ofMessage msg )
{

}

//--------------------------------------------------------------
void GUIApp::dragEvent( ofDragInfo dragInfo )
{

}
*/

void GUIApp::exit()
{
  //https://forum.openframeworks.cc/t/trigger-events-when-exiting-through-the-close-window-button-on-windows/24572

  // save state
  /*
  radio button states
  sweep rate
  alpha
  modes (auto/manual, combined/separate)
  frequencies (carrier, min, curr, max)
  output window i:
    position
    fullscreen?
  */
  SaveState( "autosave.json" );


  printf( "GUIApp: BYE.\n" );

  std::exit( 0 );
}
