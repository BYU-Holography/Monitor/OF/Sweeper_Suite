/*
BYU Holographic Video Monitor Sweeper Suite
Generates simple tones with the graphics card for evaluating Radio-
Frequency circuit and Acousto-Optic Modulator performance.

Copyright 2014-2018 BYU ElectroHolography Research Group

Email: byuholography@gmail.com

Attributions of external work:
* FAFERS_Technical_Font.ttf by F�bian Fafers, (C) 2010 (if included)
* ofxDatGui OpenFrameworks addon by Stephen Braitsch
* ofxJSON OpenFrameworks addon by Jeff Crouse, Christopher Baker, Andreas M�ller
* OpenFrameworks by respective contributing authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ofMain.h"

// http://www.openframeworks.cc/tutorials/graphics/shaders.html
// import the fancy new renderer
#include "ofGLProgrammableRenderer.h"
#include "ofAppGLFWWindow.h"

#include "GUIApp.h"
#include "outputApp.h"

//========================================================================
int main()
{
  ofGLWindowSettings gl_window_settings;
  gl_window_settings.setGLVersion( 3, 2 );



  // setup GUI window

  shared_ptr<ofAppBaseWindow> gui_window = ofCreateWindow( gl_window_settings );
  shared_ptr<GUIApp> gui_app( new GUIApp );



  // output windows containers

  //shared_ptr<ofAppBaseWindow> output_window0 = ofCreateWindow( gl_window_settings );
  //shared_ptr<outputApp> output_app0( new outputApp );

  std::vector<shared_ptr<ofAppBaseWindow>> output_windows;
  std::vector<shared_ptr<outputApp>> output_apps;



  // make links between the different apps, initialize other variables

  gui_app->gui_window = gui_window;
  gui_app->output_window_count = 2; // make two output windows
  gui_app->output_apps = &output_apps;

  // setup output windows and cross-link
  for( int output_app_i = 0; output_app_i < gui_app->output_window_count; output_app_i++ )
  {
    output_windows.emplace_back( ofCreateWindow( gl_window_settings ) );
    // output_apps.push_back( std::move( make_shared<outputApp>( new outputApp ) ) );
    // sematically correct, but throws Error C2664 'outputApp::outputApp(outputApp &&)': cannot convert argument 1 from 'outputApp *' to 'const outputApp &'	Sweeper_Suite	C : \Program Files( x86 )\Microsoft Visual Studio 14.0\VC\include\memory	907
    // this may come from push_back making a copy of the input
    output_apps.emplace_back( new outputApp );
    //output_app0->output_window = output_window0;
    output_apps[ output_app_i ]->output_window = output_windows[ output_app_i ];
    //output_app0->gui = gui_app;
    output_apps[ output_app_i ]->gui = gui_app;
    //output_app0->output_window_index = gui_app->output_window_count++; // returns current value then increments
    output_apps[ output_app_i ]->output_window_index = output_app_i;
    //gui_app->output_app0 = output_app0;
  }



  // run apps

  ofRunApp( gui_window, gui_app );

  gui_window->setWindowPosition( 100, 100 );
  gui_window->setWindowShape( gui_app->gui0->getWidth(), gui_app->gui0->getHeight() + gui_app->font.getLineHeight() );
  gui_window->setWindowTitle( "Sweeper Suite GUI" );

  for( int output_app_i = 0; output_app_i < gui_app->output_window_count; output_app_i++ )
  {
    ofRunApp( output_windows[ output_app_i ], output_apps[ output_app_i ] );

    output_windows[ output_app_i ]->setWindowPosition( 100, 250 );
    output_windows[ output_app_i ]->setWindowShape( 400, 400 );

    char temp_c_str[ 32 ];
    sprintf( temp_c_str, "Sweeper Suite Output %i", output_apps[ output_app_i ]->output_window_index );
    output_windows[ output_app_i ]->setWindowTitle( temp_c_str );
  }

  // run all

  ofRunMainLoop();
}