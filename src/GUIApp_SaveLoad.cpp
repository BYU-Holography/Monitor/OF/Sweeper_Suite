#include "GUIApp.h"
#include "outputApp.h" // needed to access class members
#include "ofxJSON.h"

void GUIApp::SaveState( string file_path )
{
  //gui_window;
  //output_app0->output_window;

  // save the window states
  printf( "Saving to %s... ", file_path.c_str() );

  ofxJSONElement config;

  ofPoint temp_ofPoint;



  // save GUI window state

  //config[ "gui_window" ][ "w" ] = gui_window->getWidth(); // this is determined by the widgets
  //config[ "gui_window" ][ "h" ] = gui_window->getHeight(); // this is determined by the widgets
  config[ "gui_window" ][ "x" ] = gui_window->getWindowPosition().x;
  config[ "gui_window" ][ "y" ] = gui_window->getWindowPosition().y;
  // don't check if GUI is full-screened; it never should be
  config[ "output_window_count" ] = output_window_count;



  // save GUI widget / control variable states

  // ofxDatGuiToggle* gui0_equalizerEnable;
  config[ "gui0_equalizerEnable" ] = gui0_equalizerEnable->getChecked();

  // ofxDatGuiToggle* gui0_timeCoherent;
  config[ "gui0_timeCoherent" ] = gui0_timeCoherent->getChecked();

  // ofxDatGuiDropdown* gui0_SweepMode;
  // int sweep_mode; // 0 - manual
  //                 // 1 - automatic
  config[ "sweep_mode" ] = sweep_mode; // it's a lot easier to do it with the state variable than extracting it from the widget

  // ofxDatGuiDropdown* gui0_outputModeDD;
  // int output_mode; // 0 - all three together
  //                  // 1 - all three separate
  config[ "output_mode" ] = output_mode;

  // ofxDatGuiDropdown* gui0_outputChannelDD;
  // int output_channel; // 0 - all three on R
  //                     // 1 - all three on G
  //                     // 2 - all three on B
  config[ "output_channel" ] = output_channel;

  // ofxDatGuiDropdown* gui0_mixingModeDD;
  // int shared_output_mixing_mode; // 0 - average with constant divisor
  //                                // 1 - average with changing divisor based upon sum of amplitudes
  config[ "shared_output_mixing_mode" ] = shared_output_mixing_mode;

  // ofxDatGuiButton* gui0_StopSweep;
  // no action needed

  // ofxDatGuiSlider* gui0_SweepRate;
  // float sweep_rate;
  config[ "gui0_SweepRate" ] = gui0_SweepRate->getValue();

  // ofxDatGuiSlider* gui0_SweepPercentage;
  // float sweep_percentage;
  config[ "gui0_SweepPercentage" ] = gui0_SweepPercentage->getValue();

  // ofxDatGuiToggle* gui0_RedEnable;
  config[ "gui0_RedEnable" ]   = gui0_RedEnable->getChecked();
  // ofxDatGuiToggle* gui0_GreenEnable;
  config[ "gui0_GreenEnable" ] = gui0_GreenEnable->getChecked();
  // ofxDatGuiToggle* gui0_BlueEnable;
  config[ "gui0_BlueEnable" ]  = gui0_BlueEnable->getChecked();

  // ofxDatGuiTextInput* gui0_RedCarrierFrequency;
  // double red_carrier_freq;
  config[ "gui0_RedCarrierFrequency" ]   = gui0_RedCarrierFrequency->getText();
  // ofxDatGuiTextInput* gui0_GreenCarrierFrequency;
  // double green_carrier_freq;
  config[ "gui0_GreenCarrierFrequency" ] = gui0_GreenCarrierFrequency->getText();
  // ofxDatGuiTextInput* gui0_BlueCarrierFrequency;
  // double blue_carrier_freq;
  config[ "gui0_BlueCarrierFrequency" ]  = gui0_BlueCarrierFrequency->getText();

  // ofxDatGuiSlider* gui0_RedMinFreq;
  // double red_min_freq;
  config[ "gui0_RedMinFreq" ]   = gui0_RedMinFreq->getValue();
  // ofxDatGuiSlider* gui0_GreenMinFreq;
  // double green_min_freq;
  config[ "gui0_GreenMinFreq" ] = gui0_GreenMinFreq->getValue();
  // ofxDatGuiSlider* gui0_BlueMinFreq;
  // double blue_min_freq;
  config[ "gui0_BlueMinFreq" ]  = gui0_BlueMinFreq->getValue();

  // ofxDatGuiSlider* gui0_RedFreq;
  config[ "gui0_RedFreq" ]   = gui0_RedFreq->getValue();
  // ofxDatGuiSlider* gui0_GreenFreq;
  config[ "gui0_GreenFreq" ] = gui0_GreenFreq->getValue();
  // ofxDatGuiSlider* gui0_BlueFreq;
  config[ "gui0_BlueFreq" ]  = gui0_BlueFreq->getValue();

  // ofxDatGuiSlider* gui0_RedMaxFreq;
  // double red_max_freq;
  config[ "gui0_RedMaxFreq" ]   = gui0_RedMaxFreq->getValue();
  // ofxDatGuiSlider* gui0_GreenMaxFreq;
  // double green_max_freq;
  config[ "gui0_GreenMaxFreq" ] = gui0_GreenMaxFreq->getValue();
  // ofxDatGuiSlider* gui0_BlueMaxFreq;
  // double blue_max_freq;
  config[ "gui0_BlueMaxFreq" ]  = gui0_BlueMaxFreq->getValue();

  // ofxDatGuiSlider* gui0_RedAmp;
  config[ "gui0_RedAmp" ]   = gui0_RedAmp->getValue();
  // ofxDatGuiSlider* gui0_GreenAmp;
  config[ "gui0_GreenAmp" ] = gui0_GreenAmp->getValue();
  // ofxDatGuiSlider* gui0_BlueAmp;
  config[ "gui0_BlueAmp" ]  = gui0_BlueAmp->getValue();

  // ofxDatGuiSlider* gui0_lastSlider;
  //config[ "gui0_lastSlider" ] = gui0_lastSlider;
  // ofxJSON doesn't appear to handle pointers natively
  // we could cast it to a 32-bit uint, but then what about when the program's compiled for 64-bit?
  // eh, just leave it out



  // save output window state

  for( int output_window_i = 0; output_window_i < output_window_count; output_window_i++ )
  {
    bool isFullscreened = (*output_apps)[ output_window_i ]->output_window->getWindowMode() == OF_FULLSCREEN;

    if( isFullscreened )
    {
      // use the last window settings before it was fullscreened
      config[ "output_window" ][ output_window_i ][ "w" ] = ( *output_apps )[ output_window_i ]->unfullscreened_window_state.w;
      config[ "output_window" ][ output_window_i ][ "h" ] = ( *output_apps )[ output_window_i ]->unfullscreened_window_state.h;
      config[ "output_window" ][ output_window_i ][ "x" ] = ( *output_apps )[ output_window_i ]->unfullscreened_window_state.x;
      config[ "output_window" ][ output_window_i ][ "y" ] = ( *output_apps )[ output_window_i ]->unfullscreened_window_state.y;
    }
    else
    {
      config[ "output_window" ][ output_window_i ][ "w" ] = ( *output_apps )[ output_window_i ]->output_window->getWidth();
      config[ "output_window" ][ output_window_i ][ "h" ] = ( *output_apps )[ output_window_i ]->output_window->getHeight();
      config[ "output_window" ][ output_window_i ][ "x" ] = ( *output_apps )[ output_window_i ]->output_window->getWindowPosition().x;
      config[ "output_window" ][ output_window_i ][ "y" ] = ( *output_apps )[ output_window_i ]->output_window->getWindowPosition().y;
    }

    config[ "output_window" ][ output_window_i ][ "fullscreen" ] =  isFullscreened;
  }

  config.save( file_path, true );
}

//--------------------------------------------------------------

bool GUIApp::LoadState( string file_path )
{
  double temp_double;
  //float temp_float;
  string temp_string;
  int temp_int;
  //unsigned int temp_uint;
  bool temp_bool;
  //char temp_cstr[ 128 ];



  // try to load state

  // attempt to load file

  printf( "Loading from %s...\n", file_path.c_str() );

  ofFile file( file_path );
  if( !file.exists() )
  {
    printf( "file does not exist.\n" );
    return false;
  }

  // attempt to parse file

  ofxJSONElement config;

  if( !config.open( file_path ) )
  {
    // unable to parse
    return false;
  }

  printf( "%s\n\n", config.getRawString().c_str() );



  // apply settings



  // set output_window_count
  // this is partially implemented code; if we ever move to dynamically-created outputApps,
  //   this variable will indicate how many output windows to create
  //   it's useless before then
  /*
  if( !config[ "output_window_count" ].isNull() )
  {
    int temp_uint = config[ "output_window_count" ].asUInt();

    if( temp_uint > 0 )
    {
      printf( "LOAD STATE: output_window_count: %u\n", temp_uint );

      output_window_count = temp_uint;
    }
  }
  */

  // set GUI window size

  // actually, don't, as this is determined by the widgets
  //if( !config[ "gui_window" ][ "w" ].isNull() && !config[ "gui_window" ][ "h" ].isNull() )
  //  gui_window->setWindowShape( config[ "gui_window" ][ "w" ].asInt(), config[ "gui_window" ][ "h" ].asInt() );

  // set GUI window position

  if( !config[ "gui_window" ][ "x" ].isNull() && !config[ "gui_window" ][ "y" ].isNull() )
  {
    int x = config[ "gui_window" ][ "x" ].asInt();
    int y = config[ "gui_window" ][ "y" ].asInt();

    if( x >= 0 && y >= 0 )
    {
      printf( "LOAD STATE: GUI window position: <%i,%i>\n", x, y );

      gui_window->setWindowPosition( x, y );
    }
  }



  // load GUI widget / control variable states

  // ofxDatGuiToggle* gui0_equalizerEnable;
  if( !config[ "gui0_equalizerEnable" ].isNull() )
  {
    temp_bool = config[ "gui0_equalizerEnable" ].asBool();

    printf( "LOAD STATE: gui0_equalizerEnable: %i\n", temp_bool );

    gui0_equalizerEnable->setChecked( temp_bool );
  }

  // ofxDatGuiToggle* gui0_timeCoherent;
  if( !config[ "gui0_timeCoherent" ].isNull() )
  {
    temp_bool = config[ "gui0_timeCoherent" ].asBool();

    printf( "LOAD STATE: gui0_timeCoherent: %i\n", temp_bool );

    gui0_timeCoherent->setChecked( temp_bool );
  }

  // ofxDatGuiDropdown* gui0_SweepMode;
  // int sweep_mode; // 0 - manual
  //                 // 1 - automatic
  if( !config[ "sweep_mode" ].isNull() )
  {
    temp_int = config[ "sweep_mode" ].asInt();

    if( 0 <= temp_int && temp_int <= 1 )
    {
      printf( "LOAD STATE: sweep_mode: %i\n", temp_int );

      gui0_SweepMode->select( temp_int );
      // effect respective GUI changes by reusing event handler code
      onDropdownEvent( ofxDatGuiDropdownEvent( gui0_SweepMode, gui0_SweepMode->getIndex(), temp_int ) );
    }
  }

  // ofxDatGuiDropdown* gui0_outputModeDD;
  // int output_mode; // 0 - all three together
  //                  // 1 - all three separate
  if( !config[ "output_mode" ].isNull() )
  {
    temp_int = config[ "output_mode" ].asInt();

    if( 0 <= temp_int && temp_int <= 1 )
    {
      printf( "LOAD STATE: output_mode: %i\n", temp_int );

      gui0_outputModeDD->select( temp_int );
      // effect respective GUI changes by reusing event handler code
      onDropdownEvent( ofxDatGuiDropdownEvent( gui0_outputModeDD, gui0_outputModeDD->getIndex(), temp_int ) );
    }
  }

  // ofxDatGuiDropdown* gui0_outputChannelDD;
  // int output_channel; // 0 - all three on R
  //                     // 1 - all three on G
  //                     // 2 - all three on B
  if( !config[ "output_channel" ].isNull() )
  {
    temp_int = config[ "output_channel" ].asInt();

    if( 0 <= temp_int && temp_int <= 2 )
    {
      printf( "LOAD STATE: output_channel: %i\n", temp_int );

      gui0_outputChannelDD->select( temp_int );
      // effect respective GUI changes by reusing event handler code
      onDropdownEvent( ofxDatGuiDropdownEvent( gui0_outputChannelDD, gui0_outputChannelDD->getIndex(), temp_int ) );
    }
  }

  // ofxDatGuiDropdown* gui0_mixingModeDD;
  // int shared_output_mixing_mode; // 0 - average with constant divisor
  //                                // 1 - average with changing divisor based upon sum of amplitudes
  if( !config[ "shared_output_mixing_mode" ].isNull() )
  {
    temp_int = config[ "shared_output_mixing_mode" ].asInt();

    if( 0 <= temp_int && temp_int <= 1 )
    {
      printf( "LOAD STATE: shared_output_mixing_mode: %i\n", temp_int );

      gui0_mixingModeDD->select( temp_int );
      // effect respective GUI changes by reusing event handler code
      onDropdownEvent( ofxDatGuiDropdownEvent( gui0_mixingModeDD, gui0_mixingModeDD->getIndex(), temp_int ) );
    }
  }

  // ofxDatGuiButton* gui0_StopSweep;
  // no action needed

  // ofxDatGuiSlider* gui0_SweepRate;
  // double sweep_rate;
  if( !config[ "gui0_SweepRate" ].isNull() )
  {
    temp_double = config[ "gui0_SweepRate" ].asDouble();

    if( gui0_SweepRate->getMin() <= temp_double && temp_double <= gui0_SweepRate->getMax() )
    {
      printf( "LOAD STATE: gui0_SweepRate: %.2f\n", temp_double );

      gui0_SweepRate->setValue( temp_double );
      sweep_rate = temp_double;
    }
  }

  // ofxDatGuiSlider* gui0_SweepPercentage;
  // double sweep_percentage;
  if( !config[ "gui0_SweepPercentage" ].isNull() )
  {
    temp_double = config[ "gui0_SweepPercentage" ].asDouble();

    if( gui0_SweepPercentage->getMin() <= temp_double && temp_double <= gui0_SweepPercentage->getMax() )
    {
      printf( "LOAD STATE: gui0_SweepPercentage: %.2f\n", temp_double );

      gui0_SweepPercentage->setValue( temp_double );
      sweep_percentage = temp_double;
    }
  }

  // ofxDatGuiToggle* gui0_RedEnable;
  if( !config[ "gui0_RedEnable" ].isNull() )
  {
    temp_bool = config[ "gui0_RedEnable" ].asBool();

    printf( "LOAD STATE: gui0_RedEnable: %i\n", temp_bool );

    gui0_RedEnable->setChecked( temp_bool );
  }
  // ofxDatGuiToggle* gui0_GreenEnable;
  if( !config[ "gui0_GreenEnable" ].isNull() )
  {
    temp_bool = config[ "gui0_GreenEnable" ].asBool();

    printf( "LOAD STATE: gui0_GreenEnable: %i\n", temp_bool );

    gui0_GreenEnable->setChecked( temp_bool );
  }
  // ofxDatGuiToggle* gui0_BlueEnable;
  if( !config[ "gui0_BlueEnable" ].isNull() )
  {
    temp_bool = config[ "gui0_BlueEnable" ].asBool();

    printf( "LOAD STATE: gui0_BlueEnable: %i\n", temp_bool );

    gui0_BlueEnable->setChecked( temp_bool );
  }

  // NOTE: need to set carrier frequencies FIRST before adjusting min/max/current freq sliders,
  //         because carrier freq. affects the min/max value of each of these sliders

  // ofxDatGuiTextInput* gui0_RedCarrierFrequency;
  // double red_carrier_freq;
  if( !config[ "gui0_RedCarrierFrequency" ].isNull() )
  {
    temp_string = config[ "gui0_RedCarrierFrequency" ].asString();
    temp_double = stod( temp_string );

    if( 0.0 <= temp_double)
    { 
      printf( "LOAD STATE: gui0_RedCarrierFrequency: %s\n", temp_string.c_str() );

      gui0_RedCarrierFrequency->setText( temp_string );
      red_carrier_freq = temp_double;
      // trigger respective changes
      onTextInputEvent( ofxDatGuiTextInputEvent( gui0_RedCarrierFrequency, temp_string ) );
    }
  }
  // ofxDatGuiTextInput* gui0_GreenCarrierFrequency;
  // double green_carrier_freq;
  if( !config[ "gui0_GreenCarrierFrequency" ].isNull() )
  {
    temp_string = config[ "gui0_GreenCarrierFrequency" ].asString();
    temp_double = stod( temp_string );

    if( 0.0 <= temp_double )
    {
      printf( "LOAD STATE: gui0_GreenCarrierFrequency: %s\n", temp_string.c_str() );

      gui0_GreenCarrierFrequency->setText( temp_string );
      green_carrier_freq = temp_double;
      // trigger respective changes
      onTextInputEvent( ofxDatGuiTextInputEvent( gui0_GreenCarrierFrequency, temp_string ) );
    }
  }
  // ofxDatGuiTextInput* gui0_BlueCarrierFrequency;
  // double blue_carrier_freq;
  if( !config[ "gui0_BlueCarrierFrequency" ].isNull() )
  {
    temp_string = config[ "gui0_BlueCarrierFrequency" ].asString();
    temp_double = stod( temp_string );

    if( 0.0 <= temp_double )
    {
      printf( "LOAD STATE: gui0_BlueCarrierFrequency: %s\n", temp_string.c_str() );

      gui0_BlueCarrierFrequency->setText( temp_string );
      blue_carrier_freq = temp_double;
      // trigger respective changes
      onTextInputEvent( ofxDatGuiTextInputEvent( gui0_BlueCarrierFrequency, temp_string ) );
    }
  }

  // NOTE: carrier freqs need to be set FIRST before adjusting min/max/current frequecies
  //         the carrier freqs affect the min/max values of these sliders their input values are checked against

  // ofxDatGuiSlider* gui0_RedMinFreq;
  // double red_min_freq;
  if( !config[ "gui0_RedMinFreq" ].isNull() )
  {
    temp_double = config[ "gui0_RedMinFreq" ].asDouble();

    if( gui0_RedMinFreq->getMin() <= temp_double && temp_double <= gui0_RedMinFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_RedMinFreq: %.2f\n", temp_double );

      gui0_RedMinFreq->setValue( temp_double );
      red_min_freq = temp_double;
    }
  }
  // ofxDatGuiSlider* gui0_GreenMinFreq;
  // double green_min_freq;
  if( !config[ "gui0_GreenMinFreq" ].isNull() )
  {
    temp_double = config[ "gui0_GreenMinFreq" ].asDouble();

    if( gui0_GreenMinFreq->getMin() <= temp_double && temp_double <= gui0_GreenMinFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_GreenMinFreq: %.2f\n", temp_double );

      gui0_GreenMinFreq->setValue( temp_double );
      green_min_freq = temp_double;
    }
  }
  // ofxDatGuiSlider* gui0_BlueMinFreq;
  // double blue_min_freq;
  if( !config[ "gui0_BlueMinFreq" ].isNull() )
  {
    temp_double = config[ "gui0_BlueMinFreq" ].asDouble();

    if( gui0_BlueMinFreq->getMin() <= temp_double && temp_double <= gui0_BlueMinFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_BlueMinFreq: %.2f\n", temp_double );

      gui0_BlueMinFreq->setValue( temp_double );
      blue_min_freq = temp_double;
    }
  }

  // ofxDatGuiSlider* gui0_RedFreq;
  if( !config[ "gui0_RedFreq" ].isNull() )
  {
    temp_double = config[ "gui0_RedFreq" ].asDouble();

    if( gui0_RedFreq->getMin() <= temp_double && temp_double <= gui0_RedFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_RedFreq: %.2f\n", temp_double );

      gui0_RedFreq->setValue( temp_double );
    }
  }
  // ofxDatGuiSlider* gui0_GreenFreq;
  if( !config[ "gui0_GreenFreq" ].isNull() )
  {
    temp_double = config[ "gui0_GreenFreq" ].asDouble();

    if( gui0_GreenFreq->getMin() <= temp_double && temp_double <= gui0_GreenFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_GreenFreq: %.2f\n", temp_double );

      gui0_GreenFreq->setValue( temp_double );
    }
  }
  // ofxDatGuiSlider* gui0_BlueFreq;
  if( !config[ "gui0_BlueFreq" ].isNull() )
  {
    temp_double = config[ "gui0_BlueFreq" ].asDouble();

    if( gui0_BlueFreq->getMin() <= temp_double && temp_double <= gui0_BlueFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_BlueFreq: %.2f\n", temp_double );

      gui0_BlueFreq->setValue( temp_double );
    }
  }

  // ofxDatGuiSlider* gui0_RedMaxFreq;
  // double red_max_freq;
  if( !config[ "gui0_RedMaxFreq" ].isNull() )
  {
    temp_double = config[ "gui0_RedMaxFreq" ].asDouble();

    if( gui0_RedMaxFreq->getMin() <= temp_double && temp_double <= gui0_RedMaxFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_RedMaxFreq: %.2f\n", temp_double );

      gui0_RedMaxFreq->setValue( temp_double );
      red_max_freq = temp_double;
    }
  }
  // ofxDatGuiSlider* gui0_GreenMaxFreq;
  // double green_max_freq;
  if( !config[ "gui0_GreenMaxFreq" ].isNull() )
  {
    temp_double = config[ "gui0_GreenMaxFreq" ].asDouble();

    if( gui0_GreenMaxFreq->getMin() <= temp_double && temp_double <= gui0_GreenMaxFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_GreenMaxFreq: %.2f\n", temp_double );

      gui0_GreenMaxFreq->setValue( temp_double );
      green_max_freq = temp_double;
    }
  }
  // ofxDatGuiSlider* gui0_BlueMaxFreq;
  // double blue_max_freq;
  if( !config[ "gui0_BlueMaxFreq" ].isNull() )
  {
    temp_double = config[ "gui0_BlueMaxFreq" ].asDouble();

    if( gui0_BlueMaxFreq->getMin() <= temp_double && temp_double <= gui0_BlueMaxFreq->getMax() )
    {
      printf( "LOAD STATE: gui0_BlueMaxFreq: %.2f\n", temp_double );

      gui0_BlueMaxFreq->setValue( temp_double );
      blue_max_freq = temp_double;
    }
  }

  // ofxDatGuiSlider* gui0_RedAmp;
  if( !config[ "gui0_RedAmp" ].isNull() )
  {
    temp_double = config[ "gui0_RedAmp" ].asDouble();

    if( gui0_RedAmp->getMin() <= temp_double && temp_double <= gui0_RedAmp->getMax() )
    {
      printf( "LOAD STATE: gui0_RedAmp: %.1f\n", temp_double );

      gui0_RedAmp->setValue( temp_double );
    }
  }
  // ofxDatGuiSlider* gui0_GreenAmp;
  if( !config[ "gui0_GreenAmp" ].isNull() )
  {
    temp_double = config[ "gui0_GreenAmp" ].asDouble();

    if( gui0_GreenAmp->getMin() <= temp_double && temp_double <= gui0_GreenAmp->getMax() )
    {
      printf( "LOAD STATE: gui0_GreenAmp: %.1f\n", temp_double );

      gui0_GreenAmp->setValue( temp_double );
    }
  }
  // ofxDatGuiSlider* gui0_BlueAmp;
  if( !config[ "gui0_BlueAmp" ].isNull() )
  {
    temp_double = config[ "gui0_BlueAmp" ].asDouble();

    if( gui0_BlueAmp->getMin() <= temp_double && temp_double <= gui0_BlueAmp->getMax() )
    {
      printf( "LOAD STATE: gui0_BlueAmp: %.1f\n", temp_double );

      gui0_BlueAmp->setValue( temp_double );
    }
  }

  // ofxDatGuiSlider* gui0_lastSlider;
  ///config[ "" ]=;



  // set output window(s) state

  for( int output_window_i = 0; output_window_i < output_window_count; output_window_i++ )
  {
    if( !config[ "output_window" ][ output_window_i ][ "w" ].isNull() &&
        !config[ "output_window" ][ output_window_i ][ "h" ].isNull() &&
        !config[ "output_window" ][ output_window_i ][ "x" ].isNull() &&
        !config[ "output_window" ][ output_window_i ][ "y" ].isNull() &&
        !config[ "output_window" ][ output_window_i ][ "fullscreen" ].isNull() )
    {
      int w = config[ "output_window" ][ output_window_i ][ "w" ].asInt();
      int h = config[ "output_window" ][ output_window_i ][ "h" ].asInt();
      int x = config[ "output_window" ][ output_window_i ][ "x" ].asInt();
      int y = config[ "output_window" ][ output_window_i ][ "y" ].asInt();
      bool fullscreen = config[ "output_window" ][ output_window_i ][ "fullscreen" ].asBool();

      //if( x >= 0 && y >= 0 )
      // window positions can be negative in Microsoft Windows if they're in a secondary monitor
      //   to the left of the primary monitor
      {
        printf( "LOAD STATE: output window %i position: <%i,%i>\n", output_window_i, x, y );

        ( *output_apps )[ output_window_i ]->output_window->setWindowPosition( x, y );
      }

      if( w > 0 && h > 0 )
      {
        printf( "LOAD STATE: output window %i size: [%i,%i]\n", output_window_i, w, h );

        ( *output_apps )[ output_window_i ]->output_window->setWindowShape( w, h );
      }

      if( fullscreen )
      {
        printf( "LOAD STATE: output window %i: fullscreen\n", output_window_i );
      }

      ( *output_apps )[ output_window_i ]->output_window->setFullscreen( fullscreen );

      ( *output_apps )[ output_window_i ]->unfullscreened_window_state.x = x;
      ( *output_apps )[ output_window_i ]->unfullscreened_window_state.y = y;
      ( *output_apps )[ output_window_i ]->unfullscreened_window_state.w = w;
      ( *output_apps )[ output_window_i ]->unfullscreened_window_state.h = h;
    }
  }

  return true;
}