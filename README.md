# BYU Holographic Video Monitor Sweeper Suite

Basic signal generator meant to generate single tones with a capable analog-output graphics card.

It has two primary objectives (but can be used elsewhere when useful):
 1) to characterize Radio-Frequency circuit board (frequency responses), and 
 2) to characterize Acousto-Optic Modulator behavior and Holographic Video Monitor optics alignment.

It has at least four windows:
 1) a debug message console window, which accepts no input
 2) a GUI, which accepts both mouse and keyboard input when in focus
 3) at least two output windows, one to fullscreen on the desired GPU output, the other for the user's reference.

It is very easy to increase/decrease the number of output windows. Simply modify the following line in "main.cpp":
```
gui_app->output_window_count = 2;
```
to specify a new number of output windows. The idea behind multiple output windows is to be able to drive multiple GPU outputs simultaneously for AOMs with many channels.

The Sweeper Suite has multiple modes of operation:
 * SWEEP: AUTO/MANUAL
   *  AUTO SWEEP: For sweeping the frequencies in unison. Each of the individual frequencies has a minimum/maximum range, and is determined by a sweep percentage (SWEEP ALPHA). A SWEEP RATE can be specified to automatically increase/decrease the SWEEP ALPHA.
   *  MANUAL SWEEP: For adjusting each of the individual frequencies independently.
 * OUTPUT MODE: SEPARATE/SHARED
   * SEPARATE OUTPUT: Each of the three frequencies is routed to its own individual GPU DAC output.
   * SHARED OUTPUT: Each of the three frequencies is combined into a specified GPU DAC output (OUTPUT CHANNEL: RED/GREEN/BLUE). An additional mode, AVERAGING DIVISOR, is available for determining the manner of combining the three signals:
     * CONSTANT AVERAGING: The three signals are added together and divided by 3. Their dynamic ranges are guaranteed to be 1/3rd their maximum range.
     * SUMMATION AVERAGING: The three signals are added together and divided by the sum of their amplitudes, guaranteeing the combined output traverses the full dynamic range of the output GPU DAC.

Each signal has the following attributes:
 * ENABLE: Enables/disables the output of this signal.
 * CARRIER FREQ: Specifies the carrier frequency if RF upconversion is used. If upconversion is not used, set to zero.
 * MIN FREQ: The minimum frequency FREQ becomes in AUTO SWEEP MODE.
 * FREQ: The current output frequency.
 * MAX FREQ: The maximum frequency FREQ becomes in AUTO SWEEP MODE.
 * AMP: Amplitude, ranging between 0.0-1.0.

This program was written for analog-output-capable NVIDIA (R) graphics cards, which typically have a GPU DAC/sample/pixel clock rate of 400MHz. This results in a radio-frequency bandwidth of 200MHz. Thus, the frequency sliders have a range of [ `*_carrier_freq`, `*_carrier_freq` + `GPU_BW_MHz` ], where `*` is either `red`/`green`/`blue`. `GPU_BW_MHz` is `#define`'d in "GUIApp.h" as `200.0`. This should be adjusted if the GPU pixel clock is different.

Signals are referenced by the GPU DAC output they are emitted from in SEPARATE MODE. (IE, the "red" signal is emitted from the red DAC in this mode.) The idea is that X signal controls X DAC output which drives the AOM steering X color of light. For tri-color (FDCM) AOMs, the color of the DAC output looses its color significance.

The Sweeper Suite saves its complete state (GUI widgets, frequencies, amplitudes, modes, window positions/fullscreened state, etc...) when the user presses [ESC] or presses the close button on the GUI window or an output window. The state is automatically saved to "autosave.json" in the "data" folder.

Keypresses for when the GUI is in focus:
 * LEFT - decrease last adjusted slider by 1%
 * RIGHT - increase last adjusted slider by 1%
 * CTRL/ALT/SHIFT - each additional modifier multiplies the LEFT/RIGHT delta by 0.1. Thus, holding down all three modifiers while pressing either LEFT/RIGHT effects a change of 0.001% of the slider's range.

Keypresses for when an output window is in focus:
 * 'f' - toggle fullscreen of this output window.
 
NOTE: If not yet implemented, you'll need to edit the ofxDatGui source file "ofxDatGui/src/components/ofxDatGuiSlider.h" to include the following functions:

```
double getScale()
{
  return mScale;
}

double getMax() // added
{
  return mMax;
}

double getMin() // added
{
  return mMin;
}
```

Implemented with:
 * OpenFrameworks v0.9.3
 * OpenFrameworks addon "ofxDatGui" by Stephen Braitsch
 * OpenFrameworks addon "ofxJSON" by Jeff Crouse, Christopher Baker, Andreas Müller
 * Fafers Technical Font by Fábian Fafers, (C) 2010 (if included)
 * Microsoft Visual Studio Professional 2015
 * Microsoft Windows 7 / 10
 * NVIDIA (R) Quadro (R) FX 5800 graphics card, for its dual DVI-__**I**__ outputs

NOTE: FAFERS_Technical_Font.ttf can be freely downloaded from several font websites, but until written permission has been received from the author, it is not included with this distribution (it will be if an enabling response is received). You can download the file and copy it into "/bin/data/" if you wish.
 
# License

All files in this repository, including source code and auxiliary references, with the exception of the below attributions of external works, are Copyright 2014-2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)).

Attributions of external work:
 * FAFERS_Technical_Font.ttf by Fábian Fafers, (C) 2010 (if included)
 * ofxDatGui OpenFrameworks addon source code by Stephen Braitsch
 * ofxJSON OpenFrameworks addon source code by Jeff Crouse, Christopher Baker, Andreas Müller
 * OpenFrameworks source code by respective contributing authors

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
