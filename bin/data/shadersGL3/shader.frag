#version 150

#define _PI 3.14159265359
#define _PI_INV 0.31830988618379067153776752674503
// _PI_INV = 1/pi = 0.31830988618379067153776752674503


#define HTOTAL 3984

const int equalizer_n = 81;
const float equalizer[81] = float[]( 1, 1, 1, 1, 0.967163726650019, 0.847227414140596, 0.798913941667404, 0.752488730173584, 0.737055175337935, 0.727779804536824, 0.710395170002956, 0.72694239921412, 0.771791515585012, 0.803526122185617, 0.848203382452404, 0.861985578705759, 0.873976992590589, 0.856051711720095, 0.872971368388112, 0.842364391438402, 0.826989508567932, 0.778036551039804, 0.751622894018205, 0.738754262993636, 0.719448978003699, 0.718621158380458, 0.72443596007499, 0.72694239921412, 0.758577575029184, 0.778036551039804, 0.838494078621807, 0.889201117857949, 0.939723310564638, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.988553094656939, 0.93003651475396, 0.925763385120825, 0.973867785328633, 0.978362954784595, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 );



out vec4 outputColor;

layout( pixel_center_integer ) in vec4 gl_FragCoord;
//vec4 - a struct of 4 floats
//dvec4 - a struct of 4 doubles

//uniform float window_w;
uniform int window_h;

uniform int equalizer_enabled;
uniform int timeCoherent;

uniform float vertGratingKR;
uniform float vertGratingKG;
uniform float vertGratingKB;

uniform float amplitudeR; // anywhere between 0.0 and 1.0
uniform float amplitudeG;
uniform float amplitudeB;

uniform float enableR; // either 0.0 or 1.0
uniform float enableG;
uniform float enableB;

uniform int outputMode;
uniform int outputChannel;
uniform int mixingMode;


void main()
{
  // gl_FragCoord contains the window relative coordinate for the fragment.
  // note that all r, g, b, a values are between 0 and 1.

  int index;

  if( timeCoherent == 0 )
  {
    index = int( gl_FragCoord.x );
  }
  else
  {
    index = int( gl_FragCoord.x ) + ( ( window_h - 1 ) - int( gl_FragCoord.y ) ) * HTOTAL;
    // use HTotal instead of HActive so that signals don't get compressed; just gated by the blanking
  }

  float equalizer_scalar_r;
  float equalizer_scalar_g;
  float equalizer_scalar_b;

  if( equalizer_enabled == 1 )
  {
    equalizer_scalar_r = equalizer[ int( vertGratingKR * _PI_INV * (equalizer_n - 1) ) ];
    equalizer_scalar_g = equalizer[ int( vertGratingKG * _PI_INV * (equalizer_n - 1) ) ];
    equalizer_scalar_b = equalizer[ int( vertGratingKB * _PI_INV * (equalizer_n - 1) ) ];
  }
  else
  {
    equalizer_scalar_r = 1.0;
    equalizer_scalar_g = 1.0;
    equalizer_scalar_b = 1.0;
  }


  float r = ( equalizer_scalar_r * cos( index * vertGratingKR ) * 0.5 + 0.5 )* amplitudeR * enableR;
  float g = ( equalizer_scalar_g * cos( index * vertGratingKG ) * 0.5 + 0.5 )* amplitudeG * enableG;
  float b = ( equalizer_scalar_b * cos( index * vertGratingKB ) * 0.5 + 0.5 )* amplitudeB * enableB;


  switch( outputMode )
  {
  case 0:
    // mode: shared channels

    float combinedSignals = r + g + b;
    float averagingDivisor = 1.0; // default, just in case

    switch( mixingMode )
    {
    case 0:
        // averaging divisor: constant

      averagingDivisor = enableR + enableG + enableB;
      break;

    case 1:
        // averaging divisor: summation

      averagingDivisor = amplitudeR * enableR + amplitudeG * enableG + amplitudeB * enableB;
      break;

    default:
      // mode: error ( magenta / orange pattern )

      if( gl_FragCoord.x > gl_FragCoord.y )
        outputColor = vec4( 1.0, 0.0, 1.0, 1.0 );
      else
        outputColor = vec4( 1.0, 0.5, 0.0, 1.0 );
      return;
    }

    // fix possible div-by-zero
    if( averagingDivisor == 0.0 )
      averagingDivisor = 1.0;

    combinedSignals /= averagingDivisor;

    // clip
    combinedSignals = min( 1.0, max( 0.0, combinedSignals ) );

    switch( outputChannel )
    {
    case 0:
      // channel: red

      outputColor = vec4( combinedSignals, 0.0, 0.0, 1.0 );
      break;

    case 1:
      // channel: green

      outputColor = vec4( 0.0, combinedSignals, 0.0, 1.0 );
      break;

    case 2:
      // channel: blue

      outputColor = vec4( 0.0, 0.0, combinedSignals, 1.0 );
      break;

    default:
      // channel: error ( red / green pattern )

      if( gl_FragCoord.x > gl_FragCoord.y )
        outputColor = vec4( 1.0, 0.0, 0.0, 1.0 );
      else
        outputColor = vec4( 0.0, 1.0, 0.0, 1.0 );
      break;
    }

    break;

  case 1:
    // mode: separate channels

    // clip
    r = min( 1.0, max( 0.0, r ) );
    g = min( 1.0, max( 0.0, g ) );
    b = min( 1.0, max( 0.0, b ) );

    outputColor = vec4( r, g, b, 1.0 );
    break;

  default:
    // mode: error ( blue / yellow pattern )

    if( gl_FragCoord.x > gl_FragCoord.y )
      outputColor = vec4( 0.0, 0.0, 1.0, 1.0 );
    else
      outputColor = vec4( 1.0, 1.0, 0.0, 1.0 );
    break;
  }
}
